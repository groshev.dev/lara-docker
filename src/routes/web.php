<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {

    Route::get('/', function() {
        // $category_name = '';
        $data = [
            'category_name' => 'dashboard',
            'page_name' => 'analytics',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'alt_menu' => 0,
        ];
        // $pageName = 'analytics';
        return view('dashboard')->with($data);
    });

    Route::prefix('category')->group(function () {
//        Route::get('/', [AdminCategories::class, 'index'])->name('admin-category');
//        Route::get('/add', [AdminCategories::class, 'add'])->name('admin-category-add');
//        Route::post('/create', [AdminCategories::class, 'create'])->name('admin-category-save');
//        Route::get('/edit/{id}', [AdminCategories::class, 'edit'])->name('admin-category-edit');
//        Route::put('/update/{id}', [AdminCategories::class, 'update'])->name('admin-category-update');
//        Route::delete('/delete/{id}', [AdminCategories::class, 'delete'])->name('admin-category-delete');
    });

});
